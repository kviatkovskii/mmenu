import java.util.Arrays;

/**
 * Created by EGAR on 08.09.2017.
 */
public class SubMenu extends AddElement{
    public Enviroment[] subMenu(Enviroment[] enviroment){
        //----------------- подменю добавления
        try {

            System.out.println("выбирете среду которую надо добавить mobile или desctop." + '\n' + "что бы создать мобильную впишите mobile" + '\n' + "что бы создать компьютер впишите desctop"+'\n');

            String subMenu = scanner.next();

            switch (subMenu) {
                case "mobile":
                    System.out.println("Выбрана мобильная среда введите параметры");
                    AddElement addElementMobile = new AddElement();
                    enviroment = addElementMobile.addMobile(enviroment);
                    System.out.println(Arrays.toString(enviroment));
                    break;

                case "desctop":
                    System.out.println("Выбрана серверная среда введите параметры");
                    AddElement addElementDesctop = new AddElement();
                    enviroment = addElementDesctop.addDesctop(enviroment);
                    System.out.println(Arrays.toString(enviroment));
                    break;
            }
        }catch( Exception ex){
                System.out.println("Введен не правильный пункт меню либо цифра вместо буквы." +'\n'+"Будь внимателенее и вводи правильно иначе будешь вводить всё сначала , боль за боль!!!!" + '\n' );
                subMenu(enviroment);
            }
        return enviroment;
    }

}

