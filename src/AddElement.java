import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by EGAR on 07.09.2017.
 */
public class AddElement {

    //------------------- Вводим переменные для будущего обьекта-----------------
    Scanner scanner = new Scanner(System.in);
  //  Enviroment[] enviroment = new Enviroment[10];


    public Enviroment[] addMobile(Enviroment[] enviroment) {
        System.out.println("Вы брано добавить мобильную среду"+'\n');
        int j = enviroment.length;

        System.out.println("Введите имя");
        String nameApp = scanner.next();
        System.out.println("Введите ширину дислея");
        String sizeDisplay = scanner.next();
        System.out.println("Введите релиз (цифра)");
        int relis = scanner.nextInt();
        //----------  Добавляем память если массив переполнен -------------------
        if (enviroment.length <= 10) {
            Enviroment[] enviromentCopyIfBiggest = Arrays.copyOf(enviroment, (int) (enviroment.length + 1.5));
            enviroment = enviromentCopyIfBiggest;

            System.out.println("Среда добавлена+'\n'");
        }
        //----------  Добавялем обьект
        enviroment[j] = new Mobile(nameApp, sizeDisplay, relis);
        System.out.println(Arrays.toString(enviroment));
        return enviroment;
    }

    public Enviroment[] addDesctop(Enviroment[] enviroment) {
        //------------------- Вводим переменные для будущего обьекта-----------------
        System.out.println("Вы брано добавить Дестопную среду"+'\n');
        int j = enviroment.length;
        System.out.println("Введите имя проекта");
        String servic = scanner.next();
        System.out.println("Введите имя репозитария где хранится");
        String repositary = scanner.next();
        System.out.println("Введите чей?");
        String who = scanner.next();

        //----------  Добавляем память если массив переполнен -------------------
        if (enviroment.length <= 10) {
            Enviroment[] enviromentCopyIfBiggest = Arrays.copyOf(enviroment, (int) (enviroment.length + 1.5));
            enviroment = enviromentCopyIfBiggest;
        }
        //----------  Добавялем обьект в массив------------------------------------
        enviroment[j] = new Descktop(servic, repositary, who);
        System.out.println("Среда добавлена"+'\n');
        return enviroment;

    }

}
